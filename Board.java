import java.util.*;
class Board
{
    int position[][];                    // Declare board as a matrix
    int hn;                        // Heuristics of the Board
    Board()
    {
        position=new int[4][4]={0};
        hn=675;
    ///FILL                        //Create the array and initialize
the heuristics to                         infinite
    }
    Board(Board x)//copy constructor
    {
        position=new int[4][4];
        position=x;//!!!

    //FILL                         //Assign the position matrix of x
to the current Board
    hn=calculateHeuristics();            //To calculate the heuristics
of the Board
    }


    public static List generateChild (Board b)
    {
    List adj_states= new LinkedList<Board> ();
    for(int i=0;i<4;i++)
    {
    for(int j=0;j<4;j++)
    {
    if(b.position[i][j]==1&& j!=0&&j!=3)            /*If the queen is
in column 1 or column 2,
                                then position it in one column before and
                        one column after and add it to adj_states*/
    {
        Board k= new Board(b);//first col
        Board m=new Board(b);//last col
        k.position[i][j]= 0;
        k.position[i][j+1]=1;
        m.position[i][j]=0;
        m.position[i][j-1]=1;
        adj_states.add(new Board(k));
        adj_states.add(new Board(m));
    }


    else if(b.position[i][j]==1&&j==0)            // If the queen is
in the first column
    {
    //FILL
    Board k= new Board(b);//!!
    k.position[i][j]= 0;
    k.position[i][j+1]=1;
    adj_states.add(new Board(k));
    }


    else if(b.position[i][j]==1&&j==3)    //If the queen is in the last column
    {
    //FILL
    Board m=new Board(b);
    m.position[i][j]= 0;
    m.position[i][j-1]=1;
    adj_states.add(new Board(m));
    }
    }
    }
    return adj_states;
    }


    public static boolean contains( Vector closed, Board p)        /*
To check if closed contains a
Board position or not */
    {

    for(int i=0;i<closed.size();i++)
    {
        int flag=0;
        Board m= (Board)closed.get(i);
    outer:    for(int j=0;j<4;j++)
        {
            for(int k=0;k<4;k++)
            {
                if(m.position[j][k]!=p.position[j][k])
                {
                    flag=1;
                    break outer;
                }
            }
        }
        if(flag==0)
        return true;

    }
    return false;
    }

    public int calculateHeuristics()        // To calculate no_of_clashes
    {
        int heur=0, row, col, k,l;
        for (row=0;row<4;row++)
        {
            for(col=0;col<4;col++)
            {
                if(position[row][col]==1)/*If a queen is positioned at
a row and col */
                {
                    for(k=row+1;k<4;k++)
                    {
                        if(position[k][col]==1)    // checking
vertically below current row
                        heur++;
                    }
                    for(k= row+1 , l= col-1; k<=3&& l>=0; k++, l--)
                    {
                        if(position[k][l]==1)    // checking
lower(left) diagonal
                        heur++;
                    }
                    for(k= row+1 , l= col+1; k<=3&& l>=0; k++,
l++)//checking (right) diagonal
                    {
                        //FILL
                        if(position[k][l]==1)    // checking
lower(left) diagonal
                        heur++;
                    }
                }
            }
        }
    }
    return heur;
    }
    public static void sortList(List q)
    {
        for(int i=0;i<q.size()-1;i++)
        {
            for(int j=0;j<q.size()-1;j++)
            {
                Board a=q.get(i);         //Read the two consecutive elements
                Board b= q.get(i+1);
                if(a.hn>b.hn)
                {

                    Board k=q.get(j);        //Remove the jth element
                    q.add(k);            //Add it to position j+1
                }
            }
        }
    }
    public static void main(String args[])
    {
        int i,j;
        int m=new int[4][4];
        List open=new LinkedList<Board> ();
        Vector closed=new Vector<Board> ();
        List adj_states= new LinkedList<Board>();
        Board b = new Board();
        Scanner sc=new Scanner(System.in);
        System.out.println("enter the initial board position");

        //FILL                        //Take the initial board
position from user
        for(i=0;i<4;i++)
        {
            for(j=0;j<4;j++)
            {
                b.position[i][j]=sc.nextInt();
            }
        }
        open.add(b);
        while(open.isEmpty()!=true)
        {
            b=(Board)open.remove(0);
            closed.add(b);
            if (b.hn==0)
            {
                System.out.println("The board position is:");
                //FILL
                for(i=0;i<4;i++)
                {
                    for(j=0;j<4;j++)
                    {
                        System.out.print(b.position[i][j]+"\t");
                    }
                }            //Print the final board position and break
                break;
            }
            adj_states=generateChild(b);
            for(int k=0;k<adj_states.size();k++)
            {
                Board w= (Board) adj_states.get(k);
                if (closed.contains(w))            // Check if closed contains w
                open.add(w);
            }
            //FILL
            b.sortlist(open);                    //Sort the open list
        }
    }
}

